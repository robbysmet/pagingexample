package com.example.robbysmet.pagingexample.genres

data class ComponentResponse(val component: Component?)

data class Component(
  val id: String?,
  val alias: String?,
  val type: String?,
  val itemType: String?,
  val title: String?,
  val collapseStrategy: ComponentCollapseStrategy?,
  val components: MutableList<Component>?,
  val data: ComponentData?,
  val actionComponent: Component?,
  val actionUrl: String?
)

data class ComponentData(
  val items: MutableList<ComponentItem>?,
  val dataUrl: String?
)

data class ComponentCollapseStrategy(
  val type: String?,
  val startTime: String?,
  val endTime: String?
)

data class ComponentItem(
  val title: String?,
  val subTitle: String?,
  val startTime: String?,
  val endTime: String?,
  val imageUrl: String?,
  val logoImageUrl: String?,
  val logoActionComponent: Component?,
  val actionComponent: Component?
)


