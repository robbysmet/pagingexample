package com.example.robbysmet.pagingexample.genres.api

import com.example.robbysmet.pagingexample.genres.ComponentResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface SwoopService {

  @GET
  fun components(@Url url: String): Call<ComponentResponse>
}