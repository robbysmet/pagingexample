package com.example.robbysmet.pagingexample.genres.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object SwoopApi {

    val service: SwoopService by lazy {
        val builder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://www.dontcare.com")

        builder.build().create(SwoopService::class.java)
    }
}
