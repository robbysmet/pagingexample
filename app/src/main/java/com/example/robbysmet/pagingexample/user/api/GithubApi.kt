package com.example.robbysmet.pagingexample.user.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object GitHubApi {

    fun createGitHubService(): GitHubService {
        val builder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.github.com")

        return builder.build().create(GitHubService::class.java)
    }
}
