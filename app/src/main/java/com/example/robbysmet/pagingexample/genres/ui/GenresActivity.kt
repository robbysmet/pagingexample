package com.example.robbysmet.pagingexample.genres.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.robbysmet.pagingexample.R.id
import com.example.robbysmet.pagingexample.R.layout
import com.example.robbysmet.pagingexample.genres.ComponentItem
import com.example.robbysmet.pagingexample.genres.GenresViewModel

class GenresActivity : AppCompatActivity() {

  private lateinit var recyclerView: RecyclerView
  private lateinit var adapter: GenresAdapter

  private val observer: Observer<PagedList<ComponentItem>> = Observer { list ->
    adapter.submitList(list)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(layout.activity_genres)
    initRecyclerView()

    val viewModel = ViewModelProviders.of(this)
        .get(GenresViewModel::class.java)
    viewModel.genresList.observe(this, observer)
  }

  private fun initRecyclerView() {
    recyclerView = findViewById(id.genres_list)
    recyclerView.layoutManager = GridLayoutManager(this, 3).apply { orientation = LinearLayoutManager.VERTICAL }
    adapter = GenresAdapter()
    recyclerView.adapter = adapter
  }

}
