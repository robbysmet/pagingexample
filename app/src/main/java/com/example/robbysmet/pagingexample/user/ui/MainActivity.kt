package com.example.robbysmet.pagingexample.user.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.robbysmet.pagingexample.R.id
import com.example.robbysmet.pagingexample.R.layout
import com.example.robbysmet.pagingexample.user.User
import com.example.robbysmet.pagingexample.user.UserViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView

    private val userAdapter = UserAdapter()
    private val userObserver: Observer<PagedList<User>> = Observer { list ->
        userAdapter.submitList(list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        initRecyclerView()

        val viewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        viewModel.userList.observe(this, userObserver)
    }

    private fun initRecyclerView() {
        recyclerView = findViewById(id.userList)
        recyclerView.layoutManager = LinearLayoutManager(this).apply { orientation = LinearLayoutManager.VERTICAL }
        recyclerView.adapter = userAdapter
    }
}
