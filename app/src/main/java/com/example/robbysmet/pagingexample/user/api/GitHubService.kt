package com.example.robbysmet.pagingexample.user.api

import com.example.robbysmet.pagingexample.user.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GitHubService {
    @GET("/users")
    fun getUser(@Query("since") since: Long, @Query("per_page") perPage: Int): Call<List<User>>
}
