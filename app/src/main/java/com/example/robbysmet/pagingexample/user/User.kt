package com.example.robbysmet.pagingexample.user

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.v7.util.DiffUtil
import com.google.gson.annotations.SerializedName

@Entity
class User {

    @SerializedName("id")
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "user_id")
    var userId: Long = 0

    @SerializedName("login")
    @ColumnInfo(name = "first_name")
    var firstName: String? = null
    var address: String? = null

    override fun equals(obj: Any?): Boolean {
        if (obj === this)
            return true

        val user = obj as User?

        return user!!.userId == this.userId && user.firstName === this.firstName
    }

    companion object {
        var DIFF_CALLBACK: DiffUtil.ItemCallback<User> = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.userId == newItem.userId
            }

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }
        }
    }
}
