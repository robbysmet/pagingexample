package com.example.robbysmet.pagingexample.genres.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.robbysmet.pagingexample.R.id
import com.example.robbysmet.pagingexample.R.layout
import com.example.robbysmet.pagingexample.genres.ComponentItem
import com.example.robbysmet.pagingexample.genres.GenresAdapter.ItemViewHolder

var DIFF_CALLBACK: DiffUtil.ItemCallback<ComponentItem> =
  object : DiffUtil.ItemCallback<ComponentItem>() {
    override fun areItemsTheSame(
      oldItem: ComponentItem,
      newItem: ComponentItem
    ): Boolean {
      return oldItem.imageUrl == newItem.imageUrl
    }

    override fun areContentsTheSame(
      oldItem: ComponentItem,
      newItem: ComponentItem
    ): Boolean {
      return oldItem == newItem
    }
  }

class GenresAdapter : PagedListAdapter<ComponentItem, ItemViewHolder>(
    DIFF_CALLBACK
) {

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): ItemViewHolder {
    val layoutInflater = LayoutInflater.from(parent.context)
    val view = layoutInflater.inflate(layout.item_genre, parent, false)
    return ItemViewHolder(view)
  }

  override fun onBindViewHolder(
    holder: ItemViewHolder,
    position: Int
  ) {
    val item = getItem(position)
    Log.d("GenreLog", "Load item $position")
    holder.bindTo(item)
  }

  class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var logo: ImageView = itemView.findViewById(id.genre_logo)

    fun bindTo(item: ComponentItem?) {
      Glide.with(itemView.context)
          .load(item?.imageUrl)
          .into(logo)
    }
  }
}
