package com.example.robbysmet.pagingexample.user.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.robbysmet.pagingexample.R.id
import com.example.robbysmet.pagingexample.R.layout
import com.example.robbysmet.pagingexample.user.User
import com.example.robbysmet.pagingexample.user.UserAdapter.UserItemViewHolder

class UserAdapter : PagedListAdapter<User, UserItemViewHolder>(
    User.DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(layout.item_user_list, parent, false)
        return UserItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.bindTo(item)
    }

    class UserItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var userName: TextView = itemView.findViewById(id.userName)
        var userId: TextView = itemView.findViewById(id.userId)

        fun bindTo(user: User?) {
            userName.text = user?.firstName ?: "Loading.."
            userId.text = user?.userId.toString()
        }
    }
}
