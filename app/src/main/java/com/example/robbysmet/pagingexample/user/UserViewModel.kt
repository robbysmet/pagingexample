package com.example.robbysmet.pagingexample.user

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList

class UserViewModel : ViewModel() {

    var userList: LiveData<PagedList<User>>

    init {
        val dataSource = UserItemKeyedDataSource()
        val config = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(20)
                .build()

        val livePagedListBuilder = LivePagedListBuilder(object : DataSource.Factory<Long, User>() {
            override fun create(): DataSource<Long, User> {
                return dataSource
            }
        }, config)

        userList = livePagedListBuilder.build()
    }
}
