package com.example.robbysmet.pagingexample.genres

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.DataSource
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList

class GenresViewModel : ViewModel() {

    var genresList: LiveData<PagedList<ComponentItem>>

    init {
        val dataSource = GenresPagedDataSource()
        val config = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(20)
                .build()

        val livePagedListBuilder = LivePagedListBuilder(object : DataSource.Factory<String, ComponentItem>() {
            override fun create(): DataSource<String, ComponentItem> {
                return dataSource
            }
        }, config)

        genresList = livePagedListBuilder.build()
    }
}
