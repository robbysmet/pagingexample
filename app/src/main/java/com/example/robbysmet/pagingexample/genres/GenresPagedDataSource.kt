package com.example.robbysmet.pagingexample.genres

import android.arch.paging.PageKeyedDataSource
import android.util.Log
import com.example.robbysmet.pagingexample.genres.api.SwoopApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.Arrays

class GenresPagedDataSource : PageKeyedDataSource<String, ComponentItem>() {

  private val urlList =
    Arrays.asList(
        "https://api.myjson.com/bins/121py3",
        "https://api.myjson.com/bins/13pqfv",
        "https://api.myjson.com/bins/yw4fv",
        "https://api.myjson.com/bins/1fk9dn",
        "https://api.myjson.com/bins/r5hkr",
        "https://api.myjson.com/bins/qk1yz"
    )

  override fun loadBefore(
    params: LoadParams<String>,
    callback: LoadCallback<String, ComponentItem>
  ) {
    // Do nothing
  }

  override fun loadInitial(
    params: LoadInitialParams<String>,
    callback: LoadInitialCallback<String, ComponentItem>
  ) {
    Log.d("GenreLog", "Load initial")

    SwoopApi.service.components(urlList[0])
        .enqueue(object : Callback<ComponentResponse> {
          override fun onResponse(
            call: Call<ComponentResponse>,
            response: Response<ComponentResponse>
          ) {
            val items = ArrayList<ComponentItem>()

            if (response.isSuccessful && response.code() == 200) {
              response.body()
                  ?.component?.data?.items?.let { items.addAll(it) }
            }

            val dataUrl = response.body()
                ?.component?.data?.dataUrl
            Log.d("Datasource", "Intial load complete, count: ${items.size}, next page $dataUrl")
            callback.onResult(items.toList(), null, dataUrl)
          }

          override fun onFailure(
            call: Call<ComponentResponse>?,
            t: Throwable?
          ) {

          }
        })
  }

  override fun loadAfter(
    params: LoadParams<String>,
    callback: LoadCallback<String, ComponentItem?>
  ) {
    Log.d("GenreLog", "Load after ${params.key}")

    SwoopApi.service.components(params.key)
        .enqueue(object : Callback<ComponentResponse> {
          override fun onResponse(
            call: Call<ComponentResponse>,
            response: Response<ComponentResponse>
          ) {
            val items = ArrayList<ComponentItem>()
            val dataUrl = response.body()
                ?.component?.data?.dataUrl

            if (response.isSuccessful && response.code() == 200) {
              response.body()
                  ?.component?.data?.items?.let {
                items.addAll(it)
                Log.d(
                    "Datasource", "Intial load complete, count: ${items.size}, next page $dataUrl"
                )
              }
            } else {
              invalidate()
            }


            callback.onResult(
                items.toList(), dataUrl
            )
          }

          override fun onFailure(
            call: Call<ComponentResponse>?,
            t: Throwable?
          ) {

          }
        })
  }

}