package com.example.robbysmet.pagingexample.user

import android.arch.paging.ItemKeyedDataSource
import android.util.Log
import com.example.robbysmet.pagingexample.user.api.GitHubApi
import com.example.robbysmet.pagingexample.user.api.GitHubService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class UserItemKeyedDataSource : ItemKeyedDataSource<Long, User>() {

    private val gitHubService: GitHubService = GitHubApi.createGitHubService()

    override fun loadInitial(params: ItemKeyedDataSource.LoadInitialParams<Long>, callback: ItemKeyedDataSource.LoadInitialCallback<User>) {
        Log.i(TAG, "Loading Rang " + 1 + " Count " + params.requestedLoadSize)
        val gitHubUser = ArrayList<User>()
        gitHubService.getUser(0, params.requestedLoadSize).enqueue(object : Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                if (response.isSuccessful && response.code() == 200) {
                    gitHubUser.addAll(response.body())
                    callback.onResult(gitHubUser)
                } else {
                    Log.e("API CALL", response.message())
                }
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                t.localizedMessage
            }
        })
    }

    override fun loadAfter(params: ItemKeyedDataSource.LoadParams<Long>, callback: ItemKeyedDataSource.LoadCallback<User>) {
        Log.i(TAG, "Loading Rang " + params.key + " Count " + params.requestedLoadSize)
        val gitHubUser = ArrayList<User>()

        gitHubService.getUser(params.key, params.requestedLoadSize).enqueue(object : Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                if (response.isSuccessful) {
                    gitHubUser.addAll(response.body())
                    callback.onResult(gitHubUser)
                } else {
                    Log.e("API CALL", response.message())
                }
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                t.localizedMessage
            }
        })
    }

    override fun loadBefore(params: ItemKeyedDataSource.LoadParams<Long>, callback: ItemKeyedDataSource.LoadCallback<User>) {

    }

    override fun getKey(item: User): Long {
        return item.userId
    }

    companion object {
        private val TAG = "Datasource"
    }
}
